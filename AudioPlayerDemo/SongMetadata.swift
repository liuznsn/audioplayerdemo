//
//  SMetadata.swift
//  AudioPlayerDemo
//
//  Created by Leo on 3/24/17.
//  Copyright © 2017 Leo. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class SongMetadata: AVMetadataItem
{
    var title: String!
    var artist: String!
    var album: String!
    var coverImage:UIImage!
    func parserMetadata(from metadata: [AVMetadataItem]) {
        for item in metadata{
            guard let key = item.commonKey else { continue }
            switch key {
            case AVMetadataCommonKeyArtist:
                self.artist = item.stringValue
            case AVMetadataCommonKeyTitle:
                self.title = item.stringValue
            case AVMetadataCommonKeyArtwork:
                if let imageData = item.dataValue, let image = UIImage(data: imageData) {
                    self.coverImage = image
                }
            default: break
            }
        }
    }
}
