//
//  ViewController.swift
//  AudioPlayerDemo
//
//  Created by LeoLiu on 2017/3/25.
//  Copyright © 2017年 LeoLiu. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation

class ViewController: UIViewController,PlayerDelegate {

    var playOrPauseBtn:UIButton!
    var progressSlider:UISlider!
    var currentTimeLabel:UILabel!
    var durationTimeLabel:UILabel!
    var musicNameLabel:UILabel!
    var artistLabel:UILabel!
    var coverImageView:UIImageView!
    var player:Player!
    var updater : CADisplayLink!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        player = Player()
        player.delegate = self
        setupControlView()

       let url = Bundle.main.resourceURL!
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: [])
            let mp3Files = directoryContents.filter{ $0.pathExtension == "mp3" }
            let mp3FileNames = mp3Files.map{ $0.deletingPathExtension().lastPathComponent }
            player.playLists = mp3FileNames
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        updateMetadata()
        updateSlider()
        
        let scc = MPRemoteCommandCenter.shared()
        scc.togglePlayPauseCommand.addTarget(self, action: #selector(ViewController.playButton(_:)))
        scc.playCommand.addTarget(self, action: #selector(ViewController.playButton(_:)))
        scc.pauseCommand.addTarget(self, action:#selector(ViewController.playButton(_:)))
        scc.nextTrackCommand.addTarget(self, action: #selector(ViewController.nextButton(_:)))
        scc.previousTrackCommand.addTarget(self, action: #selector(ViewController.previousButton(_:)))
        scc.changePlaybackPositionCommand.isEnabled = false
        
    }
    
    //MARK: control event
    
    func playButton (_ sender: Any!) {
        self.playOrPauseBtn.isSelected = !self.playOrPauseBtn.isSelected
        if (!self.playOrPauseBtn.isSelected) {
            self.player.stop()
            return
        }
        
        startPlay()
    }
    
    func nextButton (_ sender: Any!) {
        self.player.gotoNext()
        updateMetadata()
        updateSlider()
    }
    
    func previousButton (_ sender: Any!) {
        self.player.gotoPrevious()
        updateMetadata()
        updateSlider()
    }
    
    
    func removeTimer(){
        if(self.updater != nil){
            self.updater.invalidate()
            self.updater = nil
        }
    }
    
    func progressSlideValueChange() {
        currentTimeLabel.text = self.player.audoplayer.currentTime.timeIntervalAsString("mm:ss")
    }
    
    func endSlide() {
        self.player.audoplayer.currentTime = TimeInterval(self.progressSlider.value)
        updateMetadata()
        updaterInitial()
    }
    
    //MARK: ---
    
    func startPlay() {

        if(self.player.audoplayer.currentTime > 0){
            self.player.play()
        } else{
            self.player.preparePlay()
            self.player.play()
        }
        
        progressSlider.maximumValue = Float(self.player.audoplayer.duration)
        updaterInitial()
    }
    

    func updateMetadata(){
        
        let metadata = self.player.getCurrentSongMetadata()
        self.musicNameLabel.text = metadata.title
        self.artistLabel.text = metadata.artist
        self.coverImageView.image = metadata.coverImage
        let artwork = MPMediaItemArtwork.init(boundsSize: metadata.coverImage.size, requestHandler: { (size) -> UIImage in
            return metadata.coverImage
        })
        let mpic = MPNowPlayingInfoCenter.default()
        mpic.nowPlayingInfo = [
            MPMediaItemPropertyArtist: metadata.title,
            MPMediaItemPropertyTitle: metadata.artist,
            MPMediaItemPropertyArtwork: artwork,
            MPMediaItemPropertyPlaybackDuration: self.player.audoplayer.duration,
            MPNowPlayingInfoPropertyElapsedPlaybackTime: 0,
            MPNowPlayingInfoPropertyPlaybackRate: 1
        ]
        
    }
    
    func updateSlider() {
        durationTimeLabel.text = self.player.audoplayer.duration.timeIntervalAsString("mm:ss")
        currentTimeLabel.text = self.player.audoplayer.currentTime.timeIntervalAsString("mm:ss")
        progressSlider.value = Float(self.player.audoplayer.currentTime)
    }
    
    func updaterInitial(){
        updater = CADisplayLink(target: self, selector: #selector(ViewController.updateSlider))
        updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
    }
    
    
    func soundFinished(_ sender: Any) {
        self.player.gotoNext()
        self.player.play()
        updateMetadata()
        updateSlider()
    }
    
     //MARK: UI
    
    func setupControlView() {
        
        let controlView = UIView()
        controlView.contentMode = .scaleAspectFit
        controlView.translatesAutoresizingMaskIntoConstraints = false
        
        progressSlider = UISlider()
        progressSlider.contentMode = .scaleAspectFit
        progressSlider.translatesAutoresizingMaskIntoConstraints = false
        progressSlider.setThumbImage(UIImage(named: "player_slider_playback_thumb"), for: .normal)
        progressSlider.minimumTrackTintColor = UIColor.red
        progressSlider.addTarget(self, action: #selector(ViewController.removeTimer), for: .touchDown)
        progressSlider.addTarget(self, action: #selector(ViewController.progressSlideValueChange), for: .valueChanged)
        progressSlider.addTarget(self, action: #selector(ViewController.endSlide), for: .touchUpInside)
        
        
        playOrPauseBtn = UIButton(type: .custom)
        playOrPauseBtn.contentMode = .scaleAspectFit
        playOrPauseBtn.translatesAutoresizingMaskIntoConstraints = false
        playOrPauseBtn.setImage(UIImage(named: "player_btn_play_normal"), for: .normal)
        playOrPauseBtn.setImage(UIImage(named: "player_btn_pause_normal"), for: .selected)
        playOrPauseBtn.addTarget(self, action: #selector(ViewController.playButton(_:)), for: .touchUpInside)
        
        let previousMusicBtn = UIButton(type: .custom)
        previousMusicBtn.contentMode = .scaleAspectFit
        previousMusicBtn.translatesAutoresizingMaskIntoConstraints = false
        previousMusicBtn.setImage(UIImage(named: "player_btn_pre_normal"), for: .normal)
        previousMusicBtn.setImage(UIImage(named: "player_btn_pre_highlight"), for:  .highlighted)
        previousMusicBtn.addTarget(self, action: #selector(ViewController.previousButton(_:)), for: .touchUpInside)
        
        let nextMusicBtn = UIButton(type: .custom)
        nextMusicBtn.contentMode = .scaleAspectFit
        nextMusicBtn.translatesAutoresizingMaskIntoConstraints = false
        nextMusicBtn.setImage(UIImage(named: "player_btn_next_normal"), for: .normal)
        nextMusicBtn.setImage(UIImage(named: "player_btn_next_highlight"), for: .highlighted)
        nextMusicBtn.addTarget(self, action: #selector(ViewController.nextButton(_:)), for: .touchUpInside)
        
        currentTimeLabel = UILabel()
        currentTimeLabel.text = "00:00"
        currentTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        durationTimeLabel = UILabel()
        durationTimeLabel.text = "00:00"
        durationTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        let bgBlurImageView = UIImageView()
        coverImageView = UIImageView()
        coverImageView.contentMode = .scaleAspectFit
        coverImageView.translatesAutoresizingMaskIntoConstraints = false
        
        musicNameLabel = UILabel()
        musicNameLabel.adjustsFontSizeToFitWidth = true
        musicNameLabel.textAlignment = .center
        musicNameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        artistLabel = UILabel()
        artistLabel.adjustsFontSizeToFitWidth = true
        artistLabel.textAlignment = .center
        artistLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let visualEffectView = UIVisualEffectView()
        
        controlView.addSubview(playOrPauseBtn)
        controlView.addSubview(previousMusicBtn)
        controlView.addSubview(nextMusicBtn)
        controlView.addSubview(progressSlider)
        controlView.addSubview(currentTimeLabel)
        controlView.addSubview(durationTimeLabel)
        controlView.addSubview(bgBlurImageView)
        
        self.view.addSubview(visualEffectView)
        self.view.addSubview(coverImageView)
        self.view.addSubview(musicNameLabel)
        self.view.addSubview(artistLabel)
        self.view.addSubview(controlView)
        
        let views: [String: AnyObject] = ["progressSlider": progressSlider,
                                          "playOrPauseBtn":playOrPauseBtn,
                                          "previousMusicBtn":previousMusicBtn,
                                          "currentTimeLabel":currentTimeLabel,
                                          "nextMusicBtn":nextMusicBtn,
                                          "durationTimeLabel":durationTimeLabel,
                                          "controlView":controlView,
                                          "bgBlurImageView":bgBlurImageView,
                                          "coverImageView":coverImageView,
                                          "musicNameLabel":musicNameLabel,
                                          "artistLabel":artistLabel]
        
        let controlViewVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:[controlView(==120)]-0-|",
            options: [],
            metrics: nil,
            views: views)
        
        let controlViewHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[controlView]-0-|",
            options: [],
            metrics: nil,
            views: views)
        
        let subControlViewHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-5-[currentTimeLabel(==50)]-5-[progressSlider]-5-[durationTimeLabel(==50)]-5-|",
            options: [],
            metrics: nil,
            views: views)
        
        let subControlViewVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-5-[progressSlider(==20)]",
            options: [],
            metrics: nil,
            views: views)
        
        
        let nameLabelVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-50-[musicNameLabel(==25)]-5-[artistLabel(==20)]",
            options: [],
            metrics: nil,
            views: views)
        
        let nameLabelHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:[musicNameLabel(==200)]",
            options: [],
            metrics: nil,
            views: views)
        
        let coverImageViewHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-50-[coverImageView]-50-|",
            options: [],
            metrics: nil,
            views: views)
        
        let coverImageViewVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[coverImageView]-0-|",
            options: [],
            metrics: nil,
            views: views)
        
        let musicControlViewHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:[previousMusicBtn(==60)]-20-[playOrPauseBtn(==60)]-20-[nextMusicBtn(==60)]",
            options: [],
            metrics: nil,
            views: views)
        
        let musicControlViewVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-40-[playOrPauseBtn(==60)]",
            options: [],
            metrics: nil,
            views: views)
        
        
        var allConstraints = [NSLayoutConstraint]()
        allConstraints += coverImageViewHorizontalConstraints
        allConstraints += coverImageViewVerticalConstraints
        allConstraints += controlViewVerticalConstraints
        allConstraints += controlViewHorizontalConstraints
        allConstraints += subControlViewHorizontalConstraints
        allConstraints += subControlViewVerticalConstraints
        allConstraints += musicControlViewHorizontalConstraints
        allConstraints += musicControlViewVerticalConstraints
        allConstraints += nameLabelVerticalConstraints
        allConstraints += nameLabelHorizontalConstraints
        NSLayoutConstraint.activate(allConstraints)
        
        let previousMusicBtnHeightLabelAspectConstraint = NSLayoutConstraint(item: previousMusicBtn,
                                                                             attribute: .height,
                                                                             relatedBy: .equal,
                                                                             toItem: playOrPauseBtn,
                                                                             attribute: .height,
                                                                             multiplier: 1,
                                                                             constant: 0)
        
        let previousMusicBtnAspectConstraintV = NSLayoutConstraint(item: previousMusicBtn,
                                                                   attribute: .centerY,
                                                                   relatedBy: .equal,
                                                                   toItem: playOrPauseBtn,
                                                                   attribute: .centerY,
                                                                   multiplier: 1,
                                                                   constant: 0)
        
        let playOrPauseBtnCenterXLabelAspectConstraint = NSLayoutConstraint(item: playOrPauseBtn,
                                                                            attribute: .centerX,
                                                                            relatedBy: .equal,
                                                                            toItem: controlView,
                                                                            attribute: .centerX,
                                                                            multiplier: 1,
                                                                            constant: 0)
        
        let nextMusicBtnHeightLabelAspectConstraint = NSLayoutConstraint(item: nextMusicBtn,
                                                                         attribute: .height,
                                                                         relatedBy: .equal,
                                                                         toItem: playOrPauseBtn,
                                                                         attribute: .height,
                                                                         multiplier: 1,
                                                                         constant: 0)
        
        let nextMusicBtnAspectConstraintV = NSLayoutConstraint(item: nextMusicBtn,
                                                               attribute: .centerY,
                                                               relatedBy: .equal,
                                                               toItem: playOrPauseBtn,
                                                               attribute: .centerY,
                                                               multiplier: 1,
                                                               constant: 0)
        
        self.view.addConstraint(previousMusicBtnHeightLabelAspectConstraint)
        self.view.addConstraint(previousMusicBtnAspectConstraintV)
        self.view.addConstraint(playOrPauseBtnCenterXLabelAspectConstraint)
        self.view.addConstraint(nextMusicBtnHeightLabelAspectConstraint)
        self.view.addConstraint(nextMusicBtnAspectConstraintV)
        
        let currentTimeHeightLabelAspectConstraint = NSLayoutConstraint(item: currentTimeLabel,
                                                                        attribute: .height,
                                                                        relatedBy: .equal,
                                                                        toItem: progressSlider,
                                                                        attribute: .height,
                                                                        multiplier: 1,
                                                                        constant: 0)
        
        let currentTimeLabelAspectConstraintV = NSLayoutConstraint(item: currentTimeLabel,
                                                                   attribute: .centerY,
                                                                   relatedBy: .equal,
                                                                   toItem: progressSlider,
                                                                   attribute: .centerY,
                                                                   multiplier: 1,
                                                                   constant: 0)
        
        let durationTimeLabelHeightLabelAspectConstraint = NSLayoutConstraint(item: durationTimeLabel,
                                                                              attribute: .height,
                                                                              relatedBy: .equal,
                                                                              toItem: progressSlider,
                                                                              attribute: .height,
                                                                              multiplier: 1,
                                                                              constant: 0)
        
        let durationTimeLabelAspectConstraintV = NSLayoutConstraint(item: durationTimeLabel,
                                                                    attribute: .centerY,
                                                                    relatedBy: .equal,
                                                                    toItem: progressSlider,
                                                                    attribute: .centerY,
                                                                    multiplier: 1,
                                                                    constant: 0)
        
        
        self.view.addConstraint(currentTimeHeightLabelAspectConstraint)
        self.view.addConstraint(currentTimeLabelAspectConstraintV)
        self.view.addConstraint(durationTimeLabelHeightLabelAspectConstraint)
        self.view.addConstraint(durationTimeLabelAspectConstraintV)
        
        
        let nameLabelWidthConstraint = NSLayoutConstraint(item: artistLabel,
                                                          attribute: .width,
                                                          relatedBy: .equal,
                                                          toItem: musicNameLabel,
                                                          attribute: .width,
                                                          multiplier: 1,
                                                          constant: 0)
        
        let musicNameLabelCenterXConstraint = NSLayoutConstraint(item: musicNameLabel,
                                                                 attribute: .centerX,
                                                                 relatedBy: .equal,
                                                                 toItem: self.view,
                                                                 attribute: .centerX,
                                                                 multiplier: 1,
                                                                 constant: 0)
        
        let artistNameLabelCenterXConstraint = NSLayoutConstraint(item: artistLabel,
                                                                  attribute: .centerX,
                                                                  relatedBy: .equal,
                                                                  toItem: musicNameLabel,
                                                                  attribute: .centerX,
                                                                  multiplier: 1,
                                                                  constant: 0)
        
        
        self.view.addConstraint(nameLabelWidthConstraint)
        self.view.addConstraint(musicNameLabelCenterXConstraint)
        self.view.addConstraint(artistNameLabelCenterXConstraint)
        
    }


    deinit {
        let scc = MPRemoteCommandCenter.shared()
        scc.togglePlayPauseCommand.removeTarget(self)
        scc.playCommand.removeTarget(self)
        scc.pauseCommand.removeTarget(self)
        scc.nextTrackCommand.removeTarget(self)
        scc.previousTrackCommand.removeTarget(self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension TimeInterval {
    func timeIntervalAsString(_ format : String = "dd days, hh hours, mm minutes, ss seconds, sss ms") -> String {
        var asInt   = NSInteger(self)
        let ago = (asInt < 0)
        if (ago) {
            asInt = -asInt
        }
        let ms = Int(self.truncatingRemainder(dividingBy: 1) * (ago ? -1000 : 1000))
        let s = asInt % 60
        let m = (asInt / 60) % 60
        let h = ((asInt / 3600))%24
        let d = (asInt / 86400)
        
        var value = format
        value = value.replacingOccurrences(of: "hh", with: String(format: "%0.2d", h))
        value = value.replacingOccurrences(of: "mm",  with: String(format: "%0.2d", m))
        value = value.replacingOccurrences(of: "sss", with: String(format: "%0.3d", ms))
        value = value.replacingOccurrences(of: "ss",  with: String(format: "%0.2d", s))
        value = value.replacingOccurrences(of: "dd",  with: String(format: "%d", d))
        if (ago) {
            value += " ago"
        }
        return value
    }
    
}


