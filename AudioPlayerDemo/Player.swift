

import UIKit
import AVFoundation
import MediaPlayer

protocol PlayerDelegate : class {
    func soundFinished(_ sender: Any)
}

class Player : NSObject, AVAudioPlayerDelegate {
    
     private var currentIndex = 0
     private var forever = false
     private var interruptObserver : NSObjectProtocol!
     var playLists = [String](){
        didSet {
            preparePlay()
        }
     }
     var audoplayer : AVAudioPlayer!
     weak var delegate : PlayerDelegate?
    
    
    override init() {
        super.init()
        self.interruptObserver = NotificationCenter.default.addObserver(forName:
            .AVAudioSessionInterruption, object: nil, queue: nil) {
                [weak self] n in
                let why = n.userInfo![AVAudioSessionInterruptionTypeKey] as! UInt
                let type = AVAudioSessionInterruptionType(rawValue: why)!
                if type == .began {
                    print("interruption began:\n\(n.userInfo!)")
                } else {
                    self?.audoplayer.prepareToPlay()
                    self?.audoplayer.play()
                }
        }
    }
    
    func preparePlay() {
        self.audoplayer?.delegate = nil
        self.audoplayer?.stop()
        let currentSongPath = songPath(playLists[currentIndex])
        let fileURL = URL(fileURLWithPath: currentSongPath)
        guard let player = try? AVAudioPlayer(contentsOf: fileURL) else {return}
        self.audoplayer = player
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        try? AVAudioSession.sharedInstance().setActive(true)
        self.audoplayer.prepareToPlay()
        self.audoplayer.delegate = self
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        let sess = AVAudioSession.sharedInstance()
        try? sess.setActive(false, with: .notifyOthersOnDeactivation)
        try? sess.setCategory(AVAudioSessionCategoryAmbient)
        try? sess.setActive(true)
        delegate?.soundFinished(self)
    }
    
    func stop() {
        self.audoplayer.pause()
    }
    
    func play() {
        self.audoplayer.play()
    }
    
    func gotoNext(){
        self.currentIndex = self.currentIndex < playLists.count - 1 ? self.currentIndex + 1 : 0
        if isPlaying() {
            preparePlay()
            play()
        } else {
            preparePlay()
        }
        
    }
    
    func gotoPrevious(){
        let previousIndex = self.currentIndex - 1
        self.currentIndex = previousIndex < 0 ? playLists.count - 1 : previousIndex
        if isPlaying() {
            preparePlay()
            play()
        } else {
            preparePlay()
        }
    }
    
    func getCurrentSongMetadata() -> SongMetadata {
        let currentSongPath = songPath(playLists[currentIndex])
        let fileURL = URL(fileURLWithPath: currentSongPath)
        let URLAsset = AVURLAsset(url: fileURL, options: nil)
        let musicInfoArray = URLAsset.metadata(forFormat: "org.id3")
        let metadata = SongMetadata()
        metadata.parserMetadata(from: musicInfoArray)
        return metadata
    }
    
    func isPlaying() -> Bool {
        return self.audoplayer.isPlaying
    }
    
    func songPath(_ songName:String) -> String {
         return Bundle.main.path(forResource:songName, ofType: "mp3")!
    }
    
    deinit {
        if self.interruptObserver != nil {
            NotificationCenter.default.removeObserver(self.interruptObserver)
        }
        self.audoplayer.delegate = nil
    }
    
    
}
